﻿import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt

in_fname = 'test.xlsx'
trips_sheet_name = 'trips'
cost_sheet_name = 'cost'

xl = pd.ExcelFile("test.xlsx")
trips = xl.parse(trips_sheet_name)
cost = xl.parse(cost_sheet_name)

trips['cost'] = 0
for  index, row  in trips.iterrows():
    trips.at[index,'cost'] = cost.at[row['Откуда'],row['Куда']]

print(trips.cost.sum())
print(trips.cost[trips.cost>300].sum())
print(trips.groupby(['Откуда', 'Куда']).size())

print(cost[row['Откуда']])
# ---------------------------------

# список городов
town=list(cost)
t=pd.DataFrame(index=town)

# частота 
t['p']=1
t.loc['Москва']=3

# вероятности городов 
p_towm=t['p']/sum(t['p'])

np.random.seed(13)

# создаем пустую таблицу
answ = pd.DataFrame(columns=['Откуда','Куда'])

# заполняем, выборка без повторов
for i in range(1000):
    answ.loc[len(answ)]=np.random.choice(town,2,False,p_towm)

print(answ.groupby(['Откуда']).size())
print(answ.groupby(['Куда']).size())

writer = pd.ExcelWriter('answ.xlsx',engine='openpyxl')
trips.to_excel(writer, sheet_name="trips",index=False)
cost.to_excel(writer, sheet_name="cost")
answ.to_excel(writer, sheet_name = "new_trips")

writer.save()
writer.close()


import matplotlib.pyplot as plt
plt.plot(answ.groupby('Откуда').size())
plt.show()
#trips.groupby(['Откуда', 'Куда']).size().plot.hist()
#plt.show()
