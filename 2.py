import os

def sum_dir(l_dir,n) :
    if( n<0 ) : return (0)
    l_dir=os.getcwd()+'/' + l_dir
    os.chdir(l_dir)
    r=0
#    print(os.getcwd())
    for i in os.listdir():
        if  os.path.isfile(i) :
            t = sum(1 for line in open(l_dir+'/'+i, 'r'))
        else :
            t = sum_dir(i,n-1)
#        print(i,t)
        r+=t
    return(r)

print(sum_dir("dir",int(input("number "))))
