import os

for i in range(50):
    l_path=os.getcwd()
    os.mkdir("dir",mode=0o444)
    os.chdir(l_path+"\dir")

#  создание  и запись в файл
    with open("hello.txt", "w") as hello_file:
        print((str(i+1)+"\n")*(i+1), file=hello_file)

#   смена прав
    os.chmod("hello.txt",0o444)